package com.kenpal.blinkcamera;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaActionSound;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

import com.neurosky.thinkgear.TGDevice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class BlinkCameraFragment extends Fragment
        implements View.OnClickListener, FragmentCompat.OnRequestPermissionsResultCallback {

    /**
     * 画像調整用
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }
    /** カメラパーミッション */
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_WRITE_STORAGE_PERMISSION = 1;

    private static final String FRAGMENT_DIALOG = "dialog";

    /**
     * ログ用タグ
     */
    private static final String TAG = "BlinkCameraFragment";

    /**
     * Camera state: Showing camera preview.
     */
    private static final int STATE_PREVIEW = 0;

    /**
     * Camera state: Waiting for the focus to be locked.
     */
    private static final int STATE_WAITING_LOCK = 1;

    /**
     * Camera state: Waiting for the exposure to be precapture state.
     */
    private static final int STATE_WAITING_PRECAPTURE = 2;

    /**
     * Camera state: Waiting for the exposure state to be something other than precapture.
     */
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;

    /**
     * Camera state: Picture was taken.
     */
    private static final int STATE_PICTURE_TAKEN = 4;

    /**
     * Max preview width that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_WIDTH = 1920;

    /**
     * Max preview height that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    // Bluetoothアダプター
    private BluetoothAdapter bluetoothAdapter;
    // 脳波取得デバイス
    private TGDevice tgDevice;
    // Connectボタン
    private ImageButton btnConnect;
    // Connectフラグ
    private boolean connectFlag = false;
    // 撮影中フラグ
    private boolean takePhotoFlag = false;
    // バックグラウンドフラグ
    private boolean backGroundFlag = false;

    // 脳アイコン（小）
    private ImageButton btnSmallBrain;

    // ファイル名用日付
    private Date fineNameDate = new Date();

    // フラッシュON・OFF
    private boolean flashFlag = false;
    // フラッシュON・OFFボタン
    private ImageButton btnFlash;

    // 撮影ボタン
    private ImageButton btnPicture;

    // シャッター音ボタン
    private ImageButton btnSound;
    // シャッター音用
    private MediaActionSound mSound;
    // シャッター音フラグ
    private boolean shutterSoundFlag = true;

    // 感度シークバー
    private SeekBar sensitivityBar;
    private int sensitivityValue = 50;

    // 保存先アイコン
    private ImageButton btnStorage;
    // 外部ストレージフラグ
    private boolean externalStorageFlag = false;

    // カメラ切り替えアイコン
    private ImageButton btnChange;
    // インカメラフラグ
    private boolean inCameraFlag = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // fragment再生成抑止
        setRetainInstance(true);
    }

    @Override
    public void onStart() {
        backGroundFlag = false;
        super.onStart();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter == null) {
            // Alert user that Bluetooth is not available
            Toast.makeText(getActivity(), "Bluetooth not available", Toast.LENGTH_LONG).show();
            getActivity().finish();
            return;
        }else {
            if (tgDevice == null) {
                /* create the TGDevice */
                tgDevice = new TGDevice(bluetoothAdapter, handler);
            } else {
                if (tgDevice.getState() == TGDevice.STATE_CONNECTED) {
                    Log.d("脳波デバイス","接続済み");
                } else {
                    Log.d("脳波デバイス","未接続");
                }
            }
        }

        // フラッシュボタン取得
        btnFlash = (ImageButton) getActivity().findViewById(R.id.flash);
        // Connectボタン取得
        btnConnect = (ImageButton) getActivity().findViewById(R.id.connect);
        // 脳アイコン（小）取得
        btnSmallBrain = (ImageButton) getActivity().findViewById(R.id.small_brain);
        // 撮影ボタン取得
        btnPicture = (ImageButton) getActivity().findViewById(R.id.picture);
        // 撮影ボタン非表示
        btnPicture.setVisibility(View.GONE);
        // 音ボタン取得
        btnSound = (ImageButton) getActivity().findViewById(R.id.sound);
        mSound  = new MediaActionSound();
        mSound.load(MediaActionSound.SHUTTER_CLICK);
        // 保存先アイコン取得
        btnStorage = (ImageButton) getActivity().findViewById(R.id.storage);

        // カメラ切り替えアイコン取得
        btnChange = (ImageButton) getActivity().findViewById(R.id.change);

        // シークバーEvent
        sensitivityBar = (SeekBar) getActivity().findViewById(R.id.sensitivityBar);
        sensitivityBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int oldValue;

                    public void onProgressChanged(SeekBar seekBar,
                                                  int progress, boolean fromUser) {
                        // ツマミをドラッグしたときに呼ばれる
                    }

                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // ツマミに触れたときに呼ばれる
                    }

                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // ツマミを離したときに呼ばれる
                        sensitivityValue = 100 - sensitivityBar.getProgress();
                        Log.d("まばたき感度", "「" + sensitivityValue +"」に設定");
                    }
                }
        );

        // 脳ボタン表示
        if (tgDevice.getState() == TGDevice.STATE_CONNECTED) {
            btnConnect.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_brain_on", "drawable",
                            getActivity().getPackageName())));
            btnSmallBrain.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_brain_small_on", "drawable",
                            getActivity().getPackageName())));
        } else {
            btnConnect.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_brain", "drawable",
                            getActivity().getPackageName())));
            btnSmallBrain.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_brain_small", "drawable",
                            getActivity().getPackageName())));

        }
        // フラッシュボタン表示
        if(flashFlag) {
            btnFlash.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_flash_on", "drawable",
                            getActivity().getPackageName())));
        } else {
            btnFlash.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_flash_off", "drawable",
                            getActivity().getPackageName())));
        }
        // 保存先ボタン表示
        if(externalStorageFlag) {
            btnStorage.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_sd_card", "drawable",
                            getActivity().getPackageName())));
        } else {
            btnStorage.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_myself", "drawable",
                            getActivity().getPackageName())));
        }
    }

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            Log.d(TAG, "onSurfaceTextureSizeChanged");
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            backGroundFlag = true;
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {

        }

    };

    /**
     * ID of the current {@link CameraDevice}.
     */
    private String mCameraId;

    /**
     * An {@link AutoFitTextureView} for camera preview.
     */
    private AutoFitTextureView mTextureView;

    /**
     * A {@link CameraCaptureSession } for camera preview.
     */
    private CameraCaptureSession mCaptureSession;

    /**
     * A reference to the opened {@link CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * The {@link android.util.Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its state.
     */
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = getActivity();
            if (null != activity) {
                activity.finish();
            }
        }

    };

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * An {@link ImageReader} that handles still image capture.
     */
    private ImageReader mImageReader;

    /**
     * This is the output file for our picture.
     */
    private File mFile;

    /** ファイル名フォーマット */
    private SimpleDateFormat fileNameFormat = new SimpleDateFormat("'IMG_'yyyyMMdd_HHmmssSSS'.jpg'");

    /**
     * This a callback object for the {@link ImageReader}. "onImageAvailable" will be called when a
     * still image is ready to be saved.
     */
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener
            = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
            mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), mFile));
        }

    };

    /**
     * {@link CaptureRequest.Builder} for the camera preview
     */
    private CaptureRequest.Builder mPreviewRequestBuilder;

    /**
     * {@link CaptureRequest} generated by {@link #mPreviewRequestBuilder}
     */
    private CaptureRequest mPreviewRequest;

    /**
     * The current state of camera state for taking pictures.
     *
     * @see #mCaptureCallback
     */
    private int mState = STATE_PREVIEW;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            switch (mState) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        Log.d(TAG, "afState null");
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        Log.d(TAG, "afState not null");
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            mState = STATE_PICTURE_TAKEN;
                            captureStillPicture();
                        } else {
                            runPrecaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    /**
     * Shows a {@link Toast} on the UI thread.
     *
     * @param text The message to show
     */
    private void showToast(final String text) {
        final Activity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    public static BlinkCameraFragment newInstance() {
        return new BlinkCameraFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera2_basic, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // ボタンにクリックイベントを追加
        view.findViewById(R.id.picture).setOnClickListener(this);
        view.findViewById(R.id.connect).setOnClickListener(this);
        view.findViewById(R.id.flash).setOnClickListener(this);
        view.findViewById(R.id.sound).setOnClickListener(this);
        view.findViewById(R.id.storage).setOnClickListener(this);
        view.findViewById(R.id.info).setOnClickListener(this);
        view.findViewById(R.id.change).setOnClickListener(this);

        mTextureView = (AutoFitTextureView) view.findViewById(R.id.texture);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        startBackgroundThread();

        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (mTextureView.isAvailable()) {
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private void requestCameraPermission() {
        if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            new StorageConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } else {
            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE_PERMISSION);
        }
        if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            new StorageConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } else {
            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ErrorDialog.newInstance(getString(R.string.request_camera_permission))
                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        if (requestCode == REQUEST_WRITE_STORAGE_PERMISSION) {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                ErrorDialog.newInstance(getString(R.string.request_storage_permission))
                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Sets up member variables related to camera.
     *
     * @param width  The width of available size for camera preview
     * @param height The height of available size for camera preview
     */
    private void setUpCameraOutputs(int width, int height) {
        Activity activity = getActivity();
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);

        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);
//                // フラッシュ
//                manager.setTorchMode(cameraId, flashFlag);

                // We don't use a front facing camera in this sample.
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (inCameraFlag) {
                    if (facing != null && facing == CameraCharacteristics.LENS_FACING_BACK) {
                        continue;
                    }
                } else {
                    if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                        continue;
                    }
                }

                StreamConfigurationMap map = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }

                // For still image captures, we use the largest available size.
                Size largest = Collections.max(
                        Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                        new CompareSizesByArea());
                mImageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(),
                        ImageFormat.JPEG, /*maxImages*/2);
                mImageReader.setOnImageAvailableListener(
                        mOnImageAvailableListener, mBackgroundHandler);

                // Find out if we need to swap dimension to get the preview size relative to sensor
                // coordinate.
                int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
                int sensorOrientation =
                        characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                boolean swappedDimensions = false;
                switch (displayRotation) {
                    case Surface.ROTATION_0:
                    case Surface.ROTATION_180:
                        if (sensorOrientation == 90 || sensorOrientation == 270) {
                            swappedDimensions = true;
                        }
                        break;
                    case Surface.ROTATION_90:
                    case Surface.ROTATION_270:
                        if (sensorOrientation == 0 || sensorOrientation == 180) {
                            swappedDimensions = true;
                        }
                        break;
                    default:
                        Log.e(TAG, "Display rotation is invalid: " + displayRotation);
                }

                Point displaySize = new Point();
                activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
                int rotatedPreviewWidth = width;
                int rotatedPreviewHeight = height;
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if (swappedDimensions) {
                    rotatedPreviewWidth = height;
                    rotatedPreviewHeight = width;
                    maxPreviewWidth = displaySize.y;
                    maxPreviewHeight = displaySize.x;
                }

                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }

                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }

                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                        maxPreviewHeight, largest);

                // We fit the aspect ratio of TextureView to the size of preview we picked.
//                int orientation = getResources().getConfiguration().orientation;
//                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                    mTextureView.setAspectRatio(
//                            mPreviewSize.getWidth(), mPreviewSize.getHeight());
//                } else {
//                    mTextureView.setAspectRatio(
//                            mPreviewSize.getHeight(), mPreviewSize.getWidth());
//                }

                mCameraId = cameraId;
                Log.d(TAG, "CameraId:" + mCameraId);
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            ErrorDialog.newInstance(getString(R.string.camera_error))
                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
        }
    }

    /**
     * Opens the camera specified by {@link BlinkCameraFragment#mCameraId}.
     */
    private void openCamera(int width, int height) {
        // 撮影中フラグをOFFに設定
        takePhotoFlag = false;

        // パーミッションチェック
        if (PermissionChecker.checkSelfPermission(
                getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
            return;
        }

        setUpCameraOutputs(width, height);
        configureTransform(width, height);
        Activity activity = getActivity();
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraId, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        Log.d(TAG, "createCameraPreviewSession");
        try {
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder
                    = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            Log.d(TAG, "mCameraDevice.createCaptureSession onConfigured");
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                if (!inCameraFlag) {
                                    // Auto focus should be continuous for camera preview.
                                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                            CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                    if(flashFlag) {
                                        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                                                CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
                                        Log.d(TAG, "フラッシュON");
                                    } else {
                                        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                                                CaptureRequest.CONTROL_AE_MODE_ON);
                                        Log.d(TAG, "フラッシュOFF");
                                    }
                                }
//                                mPreviewRequestBuilder.set(CaptureRequest.FLASH_MODE,
//                                        CameraMetadata.FLASH_MODE_TORCH);

                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                                        mCaptureCallback, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                            Log.d(TAG, "mCameraDevice.createCaptureSession onConfigured finish");
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            showToast("Failed");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures the necessary {@link android.graphics.Matrix} transformation to `mTextureView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        Activity activity = getActivity();
        if (null == mTextureView || null == mPreviewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        mTextureView.setTransform(matrix);
    }

    /**
     * Initiate a still image capture.
     */
    private void takePicture() {
        Log.d(TAG, "takePicture");
        // nullじゃないときのみ処理
//        if(mCaptureSession == null) {
//            takePhotoFlag = false;
//            return;
//        }
        // 現状デフォルトで鳴らしている
        if (shutterSoundFlag) {
            //シャッター音を鳴らす。
            mSound.play(MediaActionSound.SHUTTER_CLICK);
        }

        fineNameDate = new Date(System.currentTimeMillis());

        File saveDir = null;
        for (File externalDir : getActivity().getExternalFilesDirs(Environment.DIRECTORY_PICTURES)) {

            if (externalDir == null) {
                continue;
            } else {
                Log.d("ストレージ", externalDir.getPath());
            }

            // まずリムーバブルでないパスを記憶させる。
            if(!Environment.isExternalStorageRemovable(externalDir)) {
                saveDir = new File(externalDir.getPath());
            } else {
                if (externalStorageFlag) {
                    saveDir = new File(externalDir.getPath());
                    Log.d("ストレージ", "取り外し可能");
                }
            }
        }
        // 撮影時にSDカードが抜かれたら、本体保存の設定に戻す。
        if (externalStorageFlag && !Environment.isExternalStorageRemovable(saveDir)) {
            externalStorageFlag = false;
            showToast(getString(R.string.msg_sd_removed_on_release));
            btnStorage.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_myself", "drawable",
                            getActivity().getPackageName())));
        }

        // ディレクトリが存在しない場合、作成
        Log.d("ディレクトリ",saveDir.getPath());
        if (!saveDir.exists()) {
            Log.d("ディレクトリ作成", saveDir.getPath());
            saveDir.mkdir();
        }

        mFile = new File(saveDir.getPath(), fileNameFormat.format(fineNameDate));

        lockFocus();
    }

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        Log.d(TAG, "lockFocus");
        try {
            if (!inCameraFlag) {
                // This is how to tell the camera to lock focus.
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                        CameraMetadata.CONTROL_AF_TRIGGER_START);
                // Tell #mCaptureCallback to wait for the lock.
                mState = STATE_WAITING_LOCK;
            } else {
                mState = STATE_WAITING_NON_PRECAPTURE;
            }
            Log.d(TAG, "mState:" + mState);

            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mCaptureCallback} from {@link #lockFocus()}.
     */
    private void runPrecaptureSequence() {
        Log.d(TAG, "runPrecaptureSequence");
        try {
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #mCaptureCallback} from both {@link #lockFocus()}.
     */
    private void captureStillPicture() {
        Log.d(TAG, "captureStillPicture");
        try {
            final Activity activity = getActivity();
            if (null == activity || null == mCameraDevice) {
                return;
            }

            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

            if(flashFlag && !inCameraFlag) {
                captureBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
                Log.d(TAG, "フラッシュON");
            } else {
                captureBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_OFF);
//                captureBuilder.set(CaptureRequest.FLASH_MODE,
//                        CameraMetadata.FLASH_MODE_OFF);
                Log.d(TAG, "フラッシュOFF");
            }
//            captureBuilder.set(CaptureRequest.FLASH_MODE,
//                    CameraMetadata.FLASH_MODE_TORCH);

            // Orientation
            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            CameraCaptureSession.CaptureCallback CaptureCallback
                    = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {

                    int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
                    switch (displayRotation) {
                        case Surface.ROTATION_0:
                            Log.d(TAG,"rotation:0");
                            displayRotation = 90;
                            break;
                        case Surface.ROTATION_180:
                            Log.d(TAG,"rotation:180");
                            displayRotation = 0;
                            break;
                        case Surface.ROTATION_90:
                            Log.d(TAG,"rotation:90");
                            displayRotation = 0;
                            break;
                        case Surface.ROTATION_270:
                            Log.d(TAG,"rotation:270");
                            displayRotation = 180;
                            break;
                        default:
                            Log.e(TAG, "Display rotation is invalid: " + displayRotation);
                    }
                    // ギャラリーに反映
                    ContentResolver contentResolver = getActivity().getContentResolver();
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.TITLE, mFile.getName());
                    values.put(MediaStore.Images.Media.DISPLAY_NAME, mFile.getName());
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                    values.put(MediaStore.Images.Media.DATA, mFile.getPath());
                    values.put(MediaStore.Images.Media.ORIENTATION, displayRotation);
                    contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);


                    // 撮影完了メッセージ
//                    showToast("Saved: " + mFile);
                    showToast(getString(R.string.msg_blink_photo_complete));
                    unlockFocus();
                    takePhotoFlag = false;
                    Log.d("撮影","完了");

                }
            };

            mCaptureSession.stopRepeating();
            mCaptureSession.capture(captureBuilder.build(), CaptureCallback, null);
        } catch (CameraAccessException e) {
            takePhotoFlag = false;
            e.printStackTrace();
        }
    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);

            if(flashFlag && !inCameraFlag) {
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
            } else {
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_OFF);
            }

            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
            // After this, the camera will go back to the normal state of preview.
            mState = STATE_PREVIEW;
            mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            takePhotoFlag = false;
            e.printStackTrace();
        }
    }

    /**
     * クリックイベント
     *
     * 撮影アイコン、脳アイコン、フラッシュON/OFFアイコン、音ON/OFFアイコン、
     * ストレージ切り替えアイコン、インフォアイコン押下時の処理を行う。
     * @param view
     */
    @Override
    public void onClick(View view) {
//        Log.d(TAG, "onClick");
        switch (view.getId()) {
            case R.id.change: {
//                Log.d(TAG, "change");
                if(inCameraFlag) {
                    inCameraFlag = false;
                } else {
                    inCameraFlag = true;
                }
                closeCamera();
                openCamera(mTextureView.getWidth(), mTextureView.getHeight());
                break;
            }
            case R.id.picture: {
                if (takePhotoFlag) {
                    break;
                }

                takePhotoFlag = true;
                takePicture();
                break;
            }
            case R.id.connect: {
                // 機器存在チェック
                if (!bluetoothAdapter.isEnabled()) {
                    showToast(getString(R.string.msg_enable_bluetooth));
                    break;
                }

                if (connectFlag) {
                    // 切断
                    disconnect(view);
                } else {
                    // 接続
                    doStuff(view);
                }
                break;
            }
            case R.id.flash: {
                if(flashFlag) {
                    flashFlag = false;
                    btnFlash.setImageDrawable(getActivity()
                            .getDrawable(getResources().getIdentifier("ic_flash_off", "drawable",
                                    getActivity().getPackageName())));
                    showToast(getString(R.string.msg_flash_off));
//                    Log.d("カメラ", "フラッシュOFF");
                } else {
                    flashFlag = true;
                    btnFlash.setImageDrawable(getActivity()
                            .getDrawable(getResources().getIdentifier("ic_flash_on", "drawable",
                                    getActivity().getPackageName())));
                    showToast(getString(R.string.msg_flash_on));
//                    Log.d("カメラ", "フラッシュON");
                }

                closeCamera();
                openCamera(mTextureView.getWidth(), mTextureView.getHeight());

                break;
            }
            case R.id.sound: {
                if(shutterSoundFlag) {
                    shutterSoundFlag = false;
                    btnSound.setImageDrawable(getActivity()
                            .getDrawable(getResources().getIdentifier("ic_sound_off", "drawable",
                                    getActivity().getPackageName())));
                    showToast(getString(R.string.msg_shutter_sound_off));
//                    Log.d("カメラ", "シャッター音OFF");
                } else {
                    shutterSoundFlag = true;
                    btnSound.setImageDrawable(getActivity()
                            .getDrawable(getResources().getIdentifier("ic_sound_on", "drawable",
                                    getActivity().getPackageName())));
                    showToast(getString(R.string.msg_shutter_sound_on));
//                    Log.d("カメラ", "シャッター音ON");
                }
                break;
            }
            case R.id.storage: {
                if(externalStorageFlag) {
                    externalStorageFlag = false;
                    btnStorage.setImageDrawable(getActivity()
                            .getDrawable(getResources().getIdentifier("ic_myself", "drawable",
                                    getActivity().getPackageName())));
                    showToast(getString(R.string.msg_data_storage_internal));

                } else {
                    if (getActivity().getExternalFilesDirs(Environment.DIRECTORY_PICTURES) == null) {
                        showToast(getString(R.string.msg_sd_not_inserted));
                    }
                    boolean hasSD = false;
                    for (File externalDir : getActivity().getExternalFilesDirs(Environment.DIRECTORY_PICTURES)) {
                        if (externalDir != null && Environment.isExternalStorageRemovable(externalDir)) {
                            hasSD = true;
                            break;
                        }
                    }
                    if (!hasSD) {
                        showToast(getString(R.string.msg_sd_not_inserted));
                    } else {
                        externalStorageFlag = true;
                        btnStorage.setImageDrawable(getActivity()
                                .getDrawable(getResources().getIdentifier("ic_sd_card", "drawable",
                                        getActivity().getPackageName())));
                        showToast(getString(R.string.msg_data_storage_sd));
                    }
                }
                break;
            }
            case R.id.info: {
                Activity activity = getActivity();
                if (null != activity) {
                    new AlertDialog.Builder(activity)
                            .setMessage(R.string.intro_message)
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                }
                break;
            }
        }
    }

    /**
     * 脳波デバイスの接続を行います。
     * @param view
     */
    private void doStuff(View view) {

        if(tgDevice.getState() != TGDevice.STATE_CONNECTING
                && tgDevice.getState() != TGDevice.STATE_CONNECTED) {
            Log.d(TAG, "脳波デバイス接続処理");
            tgDevice.connect(false);
        }
        //tgDevice.ena
    }
    /**
     * Saves a JPEG {@link Image} into the specified {@link File}.
     */
    private static class ImageSaver implements Runnable {

        /**
         * The JPEG image
         */
        private final Image mImage;
        /**
         * The file we save the image into.
         */
        private final File mFile;

        public ImageSaver(Image image, File file) {
            mImage = image;
            mFile = file;
        }

        @Override
        public void run() {
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            FileOutputStream output = null;
            try {
                output = new FileOutputStream(mFile);
                output.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mImage.close();
                if (null != output) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    /**
     * Shows an error message dialog.
     */
    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.finish();
                        }
                    })
                    .create();
        }

    }

    /**
     * Shows OK/Cancel confirmation dialog about camera permission.
     */
    public static class CameraConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_camera_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Activity activity = parent.getActivity();
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

    /**
     * Shows OK/Cancel confirmation dialog about writing permission to external storages.
     */
    public static class StorageConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_storage_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FragmentCompat.requestPermissions(parent,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_WRITE_STORAGE_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Activity activity = parent.getActivity();
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

    /**
     * Handles messages from TGDevice
     */
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
//            Log.d("脳波", "type：" + msg.what);
            // バックグラウンド起動中はここで処理終了
            if (backGroundFlag) {
//                Log.d("脳波", "バックグラウンド起動では処理しません。");
                return;
            }
            switch (msg.what) {
                case TGDevice.MSG_STATE_CHANGE:

                    switch (msg.arg1) {
                        case TGDevice.STATE_IDLE:
                            break;
                        case TGDevice.STATE_CONNECTING:
//                            Log.d("脳波", "接続中...");
                            showToast(getString(R.string.msg_connecting));
                            break;
                        case TGDevice.STATE_CONNECTED:
                            tgDevice.start();
//                            Log.d("脳波", "接続されました");
                            showToast(getString(R.string.msg_connected));
                            // 画像切り替え
                            btnConnect.setImageDrawable(getActivity()
                                    .getDrawable(getResources().getIdentifier("ic_brain_on", "drawable",
                                            getActivity().getPackageName())));
                            btnSmallBrain.setImageDrawable(getActivity()
                                    .getDrawable(getResources().getIdentifier("ic_brain_small_on", "drawable",
                                            getActivity().getPackageName())));
                            connectFlag = true;
                            break;
                        case TGDevice.STATE_NOT_FOUND:
//                            Log.d("脳波", "デバイスが見つかりません");
                            showToast(getString(R.string.msg_device_not_found));
                            break;
                        case TGDevice.STATE_NOT_PAIRED:
//                            Log.d("脳波", "ペアリングされていません");
                            showToast(getString(R.string.msg_device_not_paired));
                            break;
                        case TGDevice.STATE_DISCONNECTED:
//                            Log.d("脳波", "切断しました");
                            showToast(getString(R.string.msg_disconnect));
                            connectFlag = false;
                    }
                    break;
                case TGDevice.MSG_RAW_MULTI:
                    break;
                case TGDevice.MSG_LOW_BATTERY:
                    showToast(getString(R.string.msg_low_battery));
                    break;
                case TGDevice.MSG_BLINK:
                    if (msg.arg1 == 0) {
                        break;
                    }
//                    Log.d("脳波", "Blink: " + msg.arg1 + "\n");

                    // Blinkが感度設定値以上の場合、写真を撮る
//                    Log.d("撮影","しきい値:"+sensitivityValue);
                    if (msg.arg1 >= sensitivityValue) {
                        // 撮影中は処理しない
                        if (takePhotoFlag) {
//                            Log.d("撮影", "処理中");
                            break;
                        }
//                        Log.d("撮影", "まばたき撮影処理");
                        takePhotoFlag = true;
                        // 写真撮影処理
                        takePicture();
                    }
                    break;
                case TGDevice.MSG_ATTENTION:
//                    Log.d("脳波", "Attention: " + msg.arg1 + "\n");
//                    break;
                case TGDevice.MSG_MEDITATION:
//                    Log.d("脳波", "Meditation: " + msg.arg1 + "\n");
                    break;
                default:
                    break;
            }

        }
    };

    /**
     * 脳波デバイスを切断します。
     * @param view
     */
    private void disconnect(View view) {
        if (tgDevice.getState() == TGDevice.STATE_CONNECTED) {
            // 切断
            tgDevice.close();
            // 画像切り替え
            btnConnect.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_brain", "drawable",
                            getActivity().getPackageName())));
            btnSmallBrain.setImageDrawable(getActivity()
                    .getDrawable(getResources().getIdentifier("ic_brain_small", "drawable",
                            getActivity().getPackageName())));
        }
    }

}
